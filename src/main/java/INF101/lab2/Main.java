package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        Main.pokemon1 = new Pokemon("Pikachu");
        Main.pokemon2 = new Pokemon("Oddish");

        Boolean isOneDefeted = false;

        while (isOneDefeted == false){
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive() == false){
                isOneDefeted = false;
                break;
            }
            pokemon2.attack(pokemon1);
            if (pokemon1.isAlive() == false){
                isOneDefeted = false;
                break;
            }

        }
    }
}
